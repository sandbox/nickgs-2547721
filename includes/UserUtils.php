<?php
class UserUtils {

  /**
   * Check duplicate functiont that queries both the name and mail for the user.
   * @param  [type] $username  String to compare against the name and mail fields in the user table.
   * @return boolean        True/False depending on if duplicate is found.
   */
  public function checkDuplicate($username) {
    // Make sure the user name isn't already taken.
    $query = db_select('users')->extend('PagerDefault');
    $query->fields('users', array('uid'));
    $query->fields('users', array('mail'));
    $query->condition(db_or()->
      condition('name', '%' . db_like($username) . '%', 'LIKE')->
      condition('mail', '%' . db_like($username) . '%', 'LIKE'));

    $uids = $query
      ->limit(15)
      ->execute()
      ->fetchCol();

    if(count($uids)) {
      return true;
    }
    return false;
  }

  /**
   * Provision a new user.
   * @param  array $fields array of fields used for acount
   * @return object         user object
   */
  public function provisionUser($fields) {
    //1. create user account based on email field.
    $role = user_role_load_by_name("spinner");
    $mail = $fields['email'];
    $password = $fields['password'];

    $account = new \stdClass();
    $account->name = $mail;
    $account->mail = $mail;
    $account->init = $mail;
    $account->pass = $password;
    $account->status = 1;
    $account->roles = array(
      DRUPAL_AUTHENTICATED_RID => 'authenticated user',
    );

    /*
    $account->field_account_fullname = array(
      'und'=>array(
        0 => array(
          'value'=>$form_state['values']['name']
          )
        )
      );
    */

    //2. Send email to user welcoming, triggered via rule on user_save.
    $user = user_save(null,(array)$account);

    //3. Log in user.
    if($user) {
      //subscribe to mailchimp newsletter:
      //mailchimp_subscribe("9787ab6ae5", $user->mail);

      if ($uid = user_authenticate($account->name, $account->pass)) {
        global $user;
        $user = user_load($uid);


        $login_array = array ('name' => $account->name);
        user_login_finalize($login_array);
      }
      else {
        watchdog('UserUtils', 'Unable to log in user %email', array('%email' => $accont->mail), WATCHDOG_ERROR, null);
        drupal_set_message("There was an error provisioning your account", "error");
      }
    }
    else {
      watchdog('UserUtils', 'Unable create user %email', array('%email' => $accont->mail), WATCHDOG_ERROR, null);
       drupal_set_message("There was a problem creating your user account", "error");
    }
  }
}
